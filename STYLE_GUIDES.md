
# Contributor Stylistic guides for HackySTL

## File names and folder structures

Use `snake_case` for module files and testing files (exclude CMake files)
Each testing unit will have it's folder and for build location use testing_folder/build
And because this project uses C++ modules, you have to build the modules **by order**, this means only `make`

## Identation style

Allman style, end of discussion.

## Naming style

For now, everything will be snake_case, possibly excluding generic parameters

## Documentation style

Just to be there, will be decided on that later