<p align = 'left'>
  <img src='https://cdn.discordapp.com/attachments/809046217370763274/818426647753654302/HackySTLlogo2.png'>
</p>

This is a project which discards any backwards compatibility
thus, it's written using the latest version of the C++ standard.
The idea came from another [project](https://github.com/LegatAbyssWalker/amazingCode) 

# Goal:
To have reimplemented a bare minimum of useful features from over a year ago

# Contributors:
- DeKrain (Implementations)
- qookie (Mostly debugging and some implementations)

# Platforms
  [Discord](https://discord.gg/dEghMASRKb)

# Clarification on clangd

Until someone helps me make clangd cooperate with C++ modules I won't be using it personally