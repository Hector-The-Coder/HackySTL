#include <stdio.h>

import hsd.Types;

int main()
{
    [[maybe_unused]] hsd::i32 val = -1;
    [[maybe_unused]] hsd::add_unsigned_t<hsd::i32> t = -1;
    printf("%d, %u\n", val, t);

    [[maybe_unused]] hsd::f16 val16 = 0.1221;
    [[maybe_unused]] hsd::f16 val32 = 1.3212;
    [[maybe_unused]] hsd::f16 val64 = 14.5423;
    [[maybe_unused]] hsd::f16 val128 = 764.3256;

    // Will warn here because i haven't done any 
    printf(
        "%Lf %Lf %Lf %Lf\n", 
        static_cast<long double>(val16), 
        static_cast<long double>(val32), 
        static_cast<long double>(val64), 
        static_cast<long double>(val128)
    );
}