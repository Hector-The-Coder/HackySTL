module hsd.Types:Detail;

namespace hsd::type_detail
{
    constexpr auto get_diff()
    {
        return static_cast<char*>(nullptr) - static_cast<char*>(nullptr);
    }

    template <typename T>
    struct remove_unsigned
    {
        using type = T;
    };

    template <>
    struct remove_unsigned<unsigned char>
    {
        using type = signed char;
    };

    template <>
    struct remove_unsigned<unsigned short>
    {
        using type = short;
    };

    template <>
    struct remove_unsigned<unsigned int>
    {
        using type = int;
    };

    template <>
    struct remove_unsigned<unsigned long>
    {
        using type = long;
    };

    template <>
    struct remove_unsigned<unsigned long long>
    {
        using type = long long;
    };

    template <typename T>
    struct add_unsigned
    {
        using type = T;
    };

    template <>
    struct add_unsigned<char>
    {
        using type = unsigned char;
    };

    template <>
    struct add_unsigned<short>
    {
        using type = unsigned short;
    };

    template <>
    struct add_unsigned<int>
    {
        using type = unsigned int;
    };

    template <>
    struct add_unsigned<long>
    {
        using type = unsigned long;
    };

    template <>
    struct add_unsigned<long long>
    {
        using type = unsigned long long;
    };
} // namespace hsd::type_detail